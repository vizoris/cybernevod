$(function() {


// Фиксированная шапка припрокрутке
$(window).scroll(function(){

  if ($(window).width() > 768) {
      var sticky = $('.header'),
          scroll = $(window).scrollTop();
      if (scroll > 100) {
          sticky.addClass('header-fixed');
      } else {
          sticky.removeClass('header-fixed');
      };
    }

   
});


$(window).resize(function() {
  if ($(window).width() <= 768) {
      $('.header').removeClass('header-fixed'); 
    }

});



// Выпадающее меню
  if ($(window).width() > 991) {
    $('.drop').hover(function() {
      $(this).children('.drop-menu').stop(true, true).toggleClass('active');
    })
}




$('.priority-menu > li').hover(function() {
  $('.priority-menu > li').removeClass('active');
  $('.priority-menu__dropdown').removeClass('active');
  $(this).addClass('active');
  $(this).children('.priority-menu__dropdown').stop(true, true).addClass('active');
})





// Показать\скрыть фильтры

const btnFilters = $('.banner-wrap .btn-filter');
const btnText = $('.banner-wrap .btn-filter span');

btnFilters.click(function() {
  $('.filters').fadeToggle('active');
  console.log($(this).children('span').text());
  $(this).children('span').innerHTML = 
  (btnText.text() === 'Показать фильтр') ? btnText.text('Скрыть фильтр')  : btnText.text('Показать фильтр');
})







// Плейсфолдер в инпутах
const FloatLabel = (() => {
  
  // add active class and placeholder 
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add('active');
    
  };
  
  // remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if(!target.value) {
      target.parentNode.classList.remove('active');
    }
     
  };  
  
  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector('input,textarea');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('blur', handleBlur);    
  };
  
  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll('.form-item');
    
    floatContainers.forEach((element) => {
      if (element.querySelector('input,textarea').value) {
          element.classList.add('active');
      }      
      
      bindEvents(element);
    });
  };
  
  return {
    init: init
  };
})();

FloatLabel.init();





// steps
$('.step-item__header--name span').click(function(){
  if(!($(this).parents('.step-item__header').next().hasClass('active'))){
    $('.step-item__body').slideUp().removeClass('active');
    $(this).parents('.step-item__header').next().slideDown().addClass('active');
  }else{
    $('.step-item__body').slideUp().removeClass('active');
  };

  if(!($(this).parents('.step-item').hasClass('active'))){
    $('.step-item').removeClass("active");
    $(this).parents('.step-item').addClass("active");
  }else{
    $(this).parents('.step-item').removeClass("active");
  };
});




// recommendation slider
 var recommendationSlider =  $('.recommendation-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      // autoplay: true,
      // autoplaySpeed:4000,
      arrows: false,
      dots: true,
     
  });
$('.recommendation-slider__prev').click(function(){
  $(recommendationSlider).slick("slickNext");
 });
 $('.recommendation-slider__next').click(function(){
  $(recommendationSlider).slick("slickPrev");
 });



 // achievements slider
 var achievementsSlider =  $('.achievements-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      // autoplay: true,
      // autoplaySpeed:4000,
      arrows: false,
      dots: true,
      variableWidth: false,
     
  });
$('.achievements-slider__prev').click(function(){
  $(achievementsSlider).slick("slickNext");
 });
 $('.achievements-slider__next').click(function(){
  $(achievementsSlider).slick("slickPrev");
 });






// achievements slider
 var testimonialsSlider =  $('.testimonials-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      // autoplay: true,
      // autoplaySpeed:4000,
      arrows: false,
      dots: true,
      variableWidth: false,
     
  });
$('.testimonials-slider__prev').click(function(){
  $(testimonialsSlider).slick("slickNext");
 });
 $('.testimonials-slider__next').click(function(){
  $(testimonialsSlider).slick("slickPrev");
 });




 // Стилизация селектов
$('select').styler();







// Прикрепляем файлы
$('.attach').each(function() { // на случай, если таких групп файлов будет больше одной
  var attach = $(this),
    fieldClass = 'attach__item', // класс поля
    attachedClass = 'attach__item--attached', // класс поля с файлом
    fields = attach.find('.' + fieldClass).length, // начальное кол-во полей
    fieldsAttached = 0; // начальное кол-во полей с файлами

  var newItem = '<div class="attach__item"><label><div class="attach__up">Добавить файл</div><input class="attach__input" type="file" name="files[]" /></label><div class="attach__delete"><i class="icon icon-cross"></i></div><div class="attach__name"></div><div class="attach__edit">Изменить</div></div>'; // разметка нового поля

  // При изменении инпута
  attach.on('change', '.attach__input', function(e) {
    var item = $(this).closest('.' + fieldClass),
      fileName = '';
    if (e.target.value) { // если value инпута не пустое
      fileName = e.target.value.split('\\').pop(); // оставляем только имя файла и записываем в переменную
    }
    if (fileName) { // если имя файла не пустое
      item.find('.attach__name').text(fileName); // подставляем в поле имя файла
      if (!item.hasClass(attachedClass)) { // если в поле до этого не было файла
        item.addClass(attachedClass); // отмечаем поле классом
        fieldsAttached++;
      }
      if (fields < 10 && fields == fieldsAttached) { // если полей меньше 10 и кол-во полей равно
        item.after($(newItem)); // добавляем новое поле
        fields++;
      }
    } else { // если имя файла пустое
      if (fields == fieldsAttached + 1) {
        item.remove(); // удаляем поле
        fields--;
      } else {
        item.replaceWith($(newItem)); // заменяем поле на "чистое"
      }
      fieldsAttached--;

      if (fields == 1) { // если поле осталось одно
        attach.find('.attach__up').text('Прикрепить файл'); // меняем текст
      }
    }
  });

  // При нажатии на "Изменить"
  attach.on('click', '.attach__edit', function() {
    $(this).closest('.attach__item').find('.attach__input').trigger('click'); // имитируем клик на инпут
  });

  // При нажатии на "Удалить"
  attach.on('click', '.attach__delete', function() {
    var item = $(this).closest('.' + fieldClass);
    if (fields > fieldsAttached) { // если полей больше, чем загруженных файлов
      item.remove(); // удаляем поле
      fields--;
    } else { // если равно
      item.after($(newItem)); // добавляем новое поле
      item.remove(); // удаляем старое
    }
    fieldsAttached--;
    if (fields == 1) { // если поле осталось одно
      attach.find('.attach__up').text('Прикрепить файл'); // меняем текст
    }
  });
});










// FansyBox
 $('.fancybox').fancybox({
  closeExisting: true,
 });


// Маска телефона
$(".phone-mask").mask("+7 (999) 99-99-999");



































// MMENU
if ($(window).width() < 1200) {
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


}







})